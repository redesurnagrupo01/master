package server;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import com.google.gson.Gson;

import utils.Candidato;
import utils.Packet;

class Trabalhador implements Runnable {
    private final BufferedReader reader;
    private final PrintWriter writer;
    private final Gson gson;
    private boolean running;
    
    Trabalhador(Socket socket) throws IOException {
        this.writer = new PrintWriter(socket.getOutputStream(), true);
        this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.gson = new Gson();
        this.running = false;
    }
    
    @Override
    public void run() {
        this.running = true;
        while(this.running) {
            try {
                String line;
                while((line = reader.readLine()) != null) {
                    Packet pkt = gson.fromJson(line, Packet.class);
                    short opcode = pkt.getOpcode();
                    switch(opcode) {
                        case Packet.GET_CANDIDATES:
                            sendCandidates();
                            break;

                        case Packet.SEND_VOTES:
                            countVotes(pkt);
                            break;
                    }
                }
            } catch(IOException ex) {
                break;
            }
        }
        System.out.println("Client terminated...");
    }

    /**
     * Realiza o envio dos candidatos aos clientes.
     */
    private void sendCandidates() {
        // Inicia o pacote com o opcode de envio de candidatos
        Packet pkt = new Packet(Packet.SEND_CANDIDATES);
        
        // Preenche o campo de dados com a lista de candidatos
        UrnaEletronicaServidor.lock.readLock().lock();
        pkt.setCandidates(UrnaEletronicaServidor.candidateArray);
        UrnaEletronicaServidor.lock.readLock().unlock();
        
        // Converte o pacote para JSON e envia
        String pktJson = gson.toJson(pkt);
        writer.println(pktJson);
    }

    /**
     * Realiza a contagem de votos do pacote.
     * @param pkt pacote recebido com os votos
     */
    private void countVotes(Packet pkt) {
        int votes;
        
        // Valida a lista de candidatos da urna
        ArrayList<Candidato> copy = (ArrayList<Candidato>) pkt.getCandidates();
        for(int i = 0; i < copy.size(); i++){
            if(copy.get(i).getCode() != UrnaEletronicaServidor.candidateArray.get(i).getCode()){
                System.out.println("Packet foi adulterado!");
                return;
            }
            if(!copy.get(i).getName().equals(UrnaEletronicaServidor.candidateArray.get(i).getName())){
                System.out.println("Packet foi adulterado!");
                return;
            }
            if(!copy.get(i).getParty().equals(UrnaEletronicaServidor.candidateArray.get(i).getParty())){
                System.out.println("Packet foi adulterado!");
                return;
            }
        }

        // Adiciona o número de votos de cada candidato
        for(int i = 0; i < copy.size(); i++){
            votes = copy.get(i).getnVotes() + UrnaEletronicaServidor.candidateArray.get(i).getnVotes();
            UrnaEletronicaServidor.candidateArray.get(i).setnVotes(votes);
        }
    }

    /**
     * Verifica se a thread está executando
     */
    protected boolean isRunning() {
        return this.running;
    }

    /**
     * Indica a finalização a execução da thread
     */
    public void terminate() {
        this.running = false;
    }
}
