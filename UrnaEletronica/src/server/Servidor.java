package server;

import utils.Pair;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

class Servidor implements Runnable {
    private final ServerSocket socket;
    private boolean running;
    private final ArrayList<Pair<Trabalhador, Thread>> clients;
    
    Servidor(ServerSocket socket) {
        this.socket = socket;
        this.running = false;
        this.clients = new ArrayList<>();
    }

    @Override
    public void run() {
        this.running = true;
        while(isRunning()) {
            try {
                // Aguarda pelo pedido de conexão de um cliente
//                System.out.println("Aguardando nova conexão...");
                Socket clientSocket = this.socket.accept();
//                System.out.println("Cliente conectado!");

                // Cria uma thread para o novo cliente
                Trabalhador client = new Trabalhador(clientSocket);
                Thread clientThread = new Thread(client);
                clientThread.start();
                clients.add(new Pair<>(client, clientThread));

            } catch (IOException ex) {
                Logger.getLogger(UrnaEletronicaServidor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        try {
            // Encerra o socket
            this.socket.close();
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Verifica se o servidor está executando
     */
    private boolean isRunning() {
        return this.running;
    }
    
    /**
     * Encerra o servidor, encerrando junto todos as threads dos clientes
     */
    void terminate() {
        for(Pair<Trabalhador, Thread> c : clients) {
            c.getFirst().terminate();
            try {
                c.getSecond().join();
            } catch (InterruptedException ex) {
                Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        this.running = false;
        UrnaEletronicaServidor.svRunning = false;
    }
}
