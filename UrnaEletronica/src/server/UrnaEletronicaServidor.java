package server;

import utils.Candidato;

import java.io.*;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UrnaEletronicaServidor {
    static ArrayList<Candidato> candidateArray;
    static boolean svRunning;
    static ReadWriteLock lock;

    /**
     * Exibe o resultado parcial das votações
     */
    private static void showPartials() throws InterruptedException {
        while(svRunning) {
            System.out.println("== Resultados parciais ==");
            lock.readLock().lock();
            candidateArray.forEach(candidato -> {
                System.out.println(candidato.getName() + ": " + candidato.getnVotes());
            });
            lock.readLock().unlock();
            System.out.println("----------------------------------------------");
            Thread.sleep(1000);
        }
    }

    /**
     * Carrega a lista de candidatos a partir do arquivo csv
     * @return Resultado da leitura do arquivo csv
     */
    private static int loadCandidates() {
        candidateArray = new ArrayList<>();
        FileReader file;
        try {
            // Abre o arquivo csv
            file = new FileReader("./candidatos.csv");
        } catch (FileNotFoundException ex) {
            System.out.println("Erro ao carregar candidatos! Lista de candidatos vazia.");
            return 1;
        }
        
        // Inicia o  buffer de leitura
        BufferedReader reader = new BufferedReader(file);
        String entry;
        try {
            // Realiza a leitura de cada entrada do arquivo
            while ((entry = reader.readLine()) != null) {
                String[] candidate = entry.split(",");
                Candidato newCandidate = new Candidato();
                newCandidate.setCode(Integer.parseInt(candidate[0]));
                newCandidate.setName(candidate[1]);
                newCandidate.setParty(candidate[2]);
                candidateArray.add(newCandidate);
            }
            // Adiciona os "candidatos" que irão realizar a contagem dos votos nulos e brancos
            Candidato nullVote = new Candidato();
            nullVote.setCode(-1);
            nullVote.setName("Nulo");
            nullVote.setParty("Nulo");
            Candidato whiteVote = new Candidato();
            whiteVote.setCode(0);
            whiteVote.setName("Branco");
            whiteVote.setParty("Branco");
            candidateArray.add(nullVote);
            candidateArray.add(whiteVote);
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }

        return 0;
    }

    public static void main(String[] args) {
        Thread serverThread = null;
        Servidor server = null;
        lock = new ReentrantReadWriteLock();
        loadCandidates();
        try {
            // Cria o socket do servidor. O servidor irá ouvir a porta 40101
            ServerSocket serverSocket = new ServerSocket(40101);
            server = new Servidor(serverSocket);
            serverThread = new Thread(server);
            serverThread.start();
            svRunning = true;

            showPartials();

            // Quando a votação terminar, podemos terminar o servidor
            server.terminate();
            serverThread.join();

        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(UrnaEletronicaServidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
