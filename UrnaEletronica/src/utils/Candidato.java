/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

public class Candidato {
    private int code;
    private String name;
    private String party;
    private int nVotes;

    public Candidato() {
        code = 0;
        nVotes = 0;
        name = "";
        party = "";
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getParty() {
        return party;
    }

    public int getnVotes() {
        return nVotes;
    }

    public void setnVotes(int nVotes) {
        this.nVotes = nVotes;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int setParty(String party) {
        if(nVotes != 0) {
            System.out.println("Votação em andamento, não é permitido alterar o partido."); // precisa msm?
            return 1;
        }
        if(party.trim().length() == 0) {
            System.out.println("Partido inválido!");
            return 2;
        }
        this.party = party;
        return 0;
    }

    public void addVote() {
        nVotes++;
    }

    @Override
    public String toString(){
        String out = "--------\nNome: " + this.name + "\nNúmero: " + this.code + "\nPartido: " + this.party + "\nNúmero de Votos: " + this.nVotes + "\n";
        return out;
    }
}
