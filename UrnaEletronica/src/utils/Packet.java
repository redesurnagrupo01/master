package utils;

import java.util.ArrayList;

public class Packet {
    public static final short UNKNOWN = -1;
    public static final short SEND_MESSAGE = 0;
    public static final short SEND_VOTES = 888;
    public static final short SEND_CANDIDATES = 998;
    public static final short GET_CANDIDATES = 999;
    private final short opcode;
    private String msg;
    private ArrayList<Candidato> cList;
    
    public Packet(short opcode) {
        if(opcode != SEND_VOTES && opcode != SEND_CANDIDATES && opcode != GET_CANDIDATES) {
            this.opcode = UNKNOWN;
        } else {
            this.opcode = opcode;
        }
    }

    public short getOpcode() {
        return opcode;
    }
    
    public boolean setCandidates(ArrayList<Candidato> cList) {
        if(opcode != SEND_CANDIDATES && opcode != SEND_VOTES)
            return false;

        this.cList = (ArrayList<Candidato>)cList.clone();
        return true;
    }

    public boolean setMessage(String msg) {
        if(opcode != SEND_MESSAGE)
            return false;

        this.msg = msg;
        return true;
    }
    
    public ArrayList<Candidato> getCandidates() {
        if(opcode != SEND_CANDIDATES && opcode != SEND_VOTES)
            return null;

        return cList;
    }
}
