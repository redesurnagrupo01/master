package client;

import com.google.gson.Gson;
import utils.Candidato;
import utils.Packet;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Cliente {
    // Definições - numero candidato: 10~99
    //              branco: 0
    //              nulo: -1
    private final HashMap<Integer, Candidato> votes;
    private ArrayList<Candidato> cList;   // Meio feio essa array list aqui, mas como varios print's estão nessa classe, fica aqui por enquanto
    private final PrintWriter writer;
    private final BufferedReader reader;
    private final Gson gson;
    private boolean initialized;
    private boolean voted;

    Cliente(Socket socket) throws IOException {
        this.votes = new HashMap<>();
        this.cList = null;
        this.writer = new PrintWriter(socket.getOutputStream(), true);
        this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.gson = new Gson();
        this.initialized = false;
        this.voted = false;
    }

    /**
     * Efetua o voto do usuário
     * @return 1 -> Lista de candidatos não requisitada
     * @return 2 -> Retorna ao menu anterior
     */
    int vote() {
        if(initialized == false)
            return 1;
        
        // Imprime a lista de candidatos para o usuário
        for (Candidato c : cList) {
            if(c.getCode() < 10)
                continue;
            System.out.println("--------");
            System.out.println("Nome: " + c.getName());
            System.out.println("Número: " + c.getCode());
            System.out.println("Partido: " + c.getParty());
        }

        // Obtém a entrada do usuário
        System.out.print("Por favor, escolha o número de um candidato ou entre 'v' para voltar: ");
        Scanner scn = new Scanner(System.in);
        String input;
        while(true) {
            // Verifica se o usuário quer voltar ao menu anterior
            input = scn.nextLine();
            if(input.equals("v")) {
                return 2;
            }
            try {
                // Realiza a validação do número
                int number = Integer.parseInt(input);
                if(number >= 10 && number <= 99) {
                    for (Candidato c : cList) {
                        if(c.getCode() == number) {
                            c.addVote();
                            this.voted = true;
                            return 0;
                        }
                    }
                }
            } catch(NumberFormatException ex) {
            }
            System.out.print("Opção inválida. Tente novamente: ");
        }
    }

    /**
     * Registra um voto em branco
     * @return 1 -> Lista de candidatos não requisitada
     */
    int whiteVote() {
        if(initialized == false)
            return 1;
        
        Candidato whiteCandidate = votes.get(0);
        whiteCandidate.setnVotes(whiteCandidate.getnVotes() + 1);
        this.voted = true;
        return 0;
    }

    /**
     * Registra um voto nulo
     * @return 1 -> Lista de candidatos não requisitada
     */
    int nullVote() {
        if(initialized == false)
            return 1;
        
        Candidato nullCandidate = votes.get(-1);
        nullCandidate.setnVotes(nullCandidate.getnVotes() + 1);
        this.voted = true;
        return 0;
    }

    /** 
     * Efetua o envio dos votos computados para o servidor
     * @return false -> falha
     */
    boolean sendVotes() {
        if(initialized == false)
            return false;

        if(voted == false){
            System.out.println("Não é possivel enviar dados sem nenhum voto.");
            return false;
        }

        // Constrói o pacote e o envia
        Packet pkt = new Packet(Packet.SEND_VOTES);
        pkt.setCandidates(cList);
        String pktData = gson.toJson(pkt);
        writer.println(pktData);
        return true;
    }

    /**
     * Efetua a requisição da lista de candidatos do servidor
     * @return false -> falha na requisição
     */
    boolean candidatesRequest() {
        if(initialized)
            return false;
        try {
            // Constrói o pacote e o envia
            Packet pkt = new Packet(Packet.GET_CANDIDATES);
            String pktJson = gson.toJson(pkt);
            writer.println(pktJson);

            // Aguarda a resposta
            String line;
            if((line = reader.readLine()) != null) {
                pkt = gson.fromJson(line, Packet.class);
                // Verifica o opcode
                if(pkt.getOpcode() != Packet.SEND_CANDIDATES)
                    return false;
                cList = pkt.getCandidates();
                for(Candidato cand : cList) {
                    votes.put(cand.getCode(), cand);
                    cand.setnVotes(0);
                }
                initialized = true;
                return true;
            }
        } catch (IOException e) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, e);
            return false;
        }
        return false;
    }

    /**
     * Encerra a execução do cliente
     */
    void terminate() throws IOException {
        writer.close();
        reader.close();
    }
}
