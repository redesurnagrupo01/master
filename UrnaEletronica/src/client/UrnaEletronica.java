/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class UrnaEletronica {
    /**
     * Exibe uma saída textual para os erros encontrados nas funções do menu
     * @param res retorno das funções
     */
    private static void printResult(int res) {
        System.out.println("--------------------------------");
        switch(res) {
            case 2:
                break;
            case 1:
                System.out.println("Não é possível votar enquanto a lista de candidatos não for carregada.");
                break;
            case 0:
                System.out.println("Voto registrado com sucesso!");
                break;
        }
    }

    /**
     * Exibe o menu para o usuário
     */
    private static short showMenu() {
        short option;
        Scanner scn = new Scanner(System.in);
        do {
            System.out.println("Por favor, escolha dentre as seguintes opções:");
            System.out.println("1. Votar");
            System.out.println("2. Votar em branco");
            System.out.println("3. Anular seu voto");
            System.out.println("4. Requisitar lista de candidatos");
            System.out.println("5. Finalizar votações e enviar resultados");
            try {
                option = Short.parseShort(scn.nextLine());
            } catch(NumberFormatException ex) {
                option = 0;
            }
        } while((option < 1) || (option > 5));
        return option;
    }

    /**
     * @param args Os argumentos de entrada do programa
     */
    public static void main(String[] args) {
        String ipAddress = "cosmos.lasdpc.icmc.usp.br";
        int portNumber = 40101;
        switch(args.length) {
            case 0:
                System.out.println("Usando as configurações padrão (ip: " + ipAddress + ", port: " + String.valueOf(portNumber));
                System.out.println("Para alterar as configurações execute o programa com os argumentos. Por exemplo: urna 192.168.0.101 40101");
                System.out.println("--------------------------------");
                break;
            case 1:
                ipAddress = args[0];
                break;
            case 2:
                portNumber = Integer.parseInt(args[1]);
            default:
                break;
        }
        
        Cliente client = null;
        try {
            System.out.println("Conectando...");
            Socket socket = new Socket(ipAddress, portNumber);
            client = new Cliente(socket);
            System.out.println("Conectado!");
            
//            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException ex) {
            System.out.println("Falha na conexão com o servidor! Verifique os argumentos.");
            System.exit(1);
        }

        boolean running = true;
        System.out.println("Bom dia! Esse é o sistema de urnas eletrônicas 'Confiabilis Urnis'! Seja bem vindo!");
        while(running) {
            int result;
            short option;
            switch (option = showMenu()) {
                case 1:
                    result = client.vote();
                    printResult(result);
                    break;
                case 2:
                    result = client.whiteVote();
                    printResult(result);
                    break;
                case 3:
                    result = client.nullVote();
                    printResult(result);
                    break;
                case 4:
                    // Bloquear requests após o inicio da votação
                    client.candidatesRequest();
                    break;
                case 5:
                    if(client.sendVotes())
                        running = false;
                    else
                        System.out.println("Falha ao enviar votos.");
            }
        }
        
        try {
            client.terminate();
        } catch (IOException ex) {
            System.out.println("Falha ao fechar stream de escrita/leitura");
        }

        System.out.println("Obrigado por utilizar o 'Urnis Confiabilis', esperamos que sua experiência tenha sido agradável");
    }
    
}
